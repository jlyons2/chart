## 0.2.0 (2021-11-18)

### 🔧 CI changes (1 change)

- [Remove docker tag from install job](dependabot-gitlab/chart@c09a964c51084e5ceeda03b1e69b67bf54daf777) ([merge request](dependabot-gitlab/chart!28))

### 📦 Dependency updates (2 changes)

- [Bump mongodb chart version](dependabot-gitlab/chart@8c4e504ed226e54c14a5176e5616e18135f92a39) ([merge request](dependabot-gitlab/chart!29))
- [Bump redis chart version](dependabot-gitlab/chart@14963d45e8aaaa08c36cb4bb2d529dbcce46cfef) ([merge request](dependabot-gitlab/chart!27))

## 0.1.3 (2021-11-18)

### 🚀 Features (1 change)

- [Update app version to v0.10.9](dependabot-gitlab/chart@af3942f30ea1c7f04cce38cd4c51e29bca6987b9)

### 🔧 CI changes (1 change)

- [Always build chart](dependabot-gitlab/chart@0099c19fcba4d1d418f2f53e2401c9ae66598b58) ([merge request](dependabot-gitlab/chart!24))

### 🛠️ Chore (1 change)

- [Publish logo in pages](dependabot-gitlab/chart@892da0d186996cb1eb8e56ff9a83d2a2e6044f31) ([merge request](dependabot-gitlab/chart!26))

## 0.1.2 (2021-11-16)

### 🔧 CI changes (2 changes)

- [Package readme in license with chart](dependabot-gitlab/chart@c1d1624556170c40961f40e6a59d26c832fe9ad8) ([merge request](dependabot-gitlab/chart!21))
- [Install grep in CI image](dependabot-gitlab/chart@114ea966b6085729a4e85f2447a0020c52f616a5) ([merge request](dependabot-gitlab/chart!20))

### 🛠️ Chore (2 changes)

- [Fix logo url](dependabot-gitlab/chart@965d68c20bddf959e5c20c22ecc962e6c4537e2b) ([merge request](dependabot-gitlab/chart!23))
- [Add artifacthub repo info](dependabot-gitlab/chart@2565e97eb4ddcdfe86426459099b0549bfdc37b1) ([merge request](dependabot-gitlab/chart!22))

## 0.1.1 (2021-11-16)

### 🐞 Bug Fixes (1 change)

- [Generate tag specific migration job name](dependabot-gitlab/chart@aa94d111306266d4ae0ce7e9e22d5ced5b497d84) ([merge request](dependabot-gitlab/chart!17))

### 🔧 CI changes (2 changes)

- [Generate changelog for releases](dependabot-gitlab/chart@9b391ee0238a029584981cebaf164a454fe36e0e) ([merge request](dependabot-gitlab/chart!15))
- [Do not publish chart if tests failed](dependabot-gitlab/chart@daed1ccbc9ef790375c0647f848f580a9a3a863b) ([merge request](dependabot-gitlab/chart!14))

### 🛠️ Chore (1 change)

- [Shorten job names](dependabot-gitlab/chart@9e4b7de92e12c95cd22dbc1396016fb90c49d453) ([merge request](dependabot-gitlab/chart!19))

### 📄 Documentation updates (1 change)

- [Update value documentation and default values](dependabot-gitlab/chart@4ee7c24998f8435665314a907b9231918f09148d) ([merge request](dependabot-gitlab/chart!16))

## 0.1.0 (2021-11-14)

### 🛠️ Chore (1 change)

- [Update release tag format and fix release script](dependabot-gitlab/chart@d740a28ee1698189511a9c3293820fc53baf08e5) ([merge request](dependabot-gitlab/chart!13))

### 📄 Documentation updates (1 change)

- [Update chart url in documentation](dependabot-gitlab/chart@94478e14d2fc75a2c1bb17b8aee8d8d9b110b1b9) ([merge request](dependabot-gitlab/chart!12))

### CI (1 change)

- [Add chart release jobs and utils](dependabot-gitlab/chart@ea1a45999523d3cd9bedb53ddbf2290e022072bd) ([merge request](dependabot-gitlab/chart!11))
