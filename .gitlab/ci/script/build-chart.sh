#!/bin/bash

set -euo pipefail

source "$(dirname "$0")/utils.sh"

log "Package helm chart"
cp README.md LICENSE "$CHART_DIR/"
helm package --dependency-update "$CHART_DIR"

log "Fetch index.yaml from $CI_PAGES_URL"
curl -f -o index.yaml "$CI_PAGES_URL/index.yaml"

log "Update index.yml"
helm repo index . --merge index.yaml --url https://storage.googleapis.com/dependabot-gitlab
mv index.yaml public/
