# dependabot-gitlab

![Version: 0.2.0](https://img.shields.io/badge/Version-0.2.0-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square) ![AppVersion: 0.10.9](https://img.shields.io/badge/AppVersion-0.10.9-informational?style=flat-square)

**Homepage:** <https://gitlab.com/dependabot-gitlab/dependabot>

*dependabot-gitlab* is application providing automated dependency updates for gitlab projects.

## Introduction

This chart bootstraps dependabot-gitlab deployment on a [Kubernetes](http://kubernetes.io) cluster using the [Helm](https://helm.sh) package manager.

## Add Helm repo

```bash
helm repo add dependabot https://dependabot-gitlab.gitlab.io/chart
```

## Installing the Chart

Install this chart using:

```bash
helm install dependabot dependabot/dependabot-gitlab --values values.yaml
```

The command deploys dependabot-gitlab on the Kubernetes cluster in the default configuration. The [values](#values) section lists the parameters that can be configured during installation.

By default, both [redis](https://github.com/bitnami/charts/tree/master/bitnami/redis) and [mongodb](https://github.com/bitnami/charts/tree/master/bitnami/mongodb) are deployed. 
These can be disabled by setting `redis.enabled: false` and `mongodb.enabled: false` in `values.yaml`

## Requirements

| Repository | Name | Version |
|------------|------|---------|
| https://charts.bitnami.com/bitnami | mongodb | ~10.29.0 |
| https://charts.bitnami.com/bitnami | redis | ~15.5.5 |

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| createProjectsJob.activeDeadlineSeconds | int | `240` | Job Active Deadline |
| createProjectsJob.backoffLimit | int | `1` | Job Back off limit |
| createProjectsJob.resources | object | `{}` | Create projects job resource definitions |
| credentials.github_access_token | string | `""` | Github access token |
| credentials.gitlab_access_token | string | `"test"` | Gitlab access token, required |
| credentials.gitlab_auth_token | string | `""` | Gitlab auth token for webhook authentication |
| env.appConfigPath | string | `"kube/config"` | Configuration path |
| env.appRootPath | string | `"/home/dependabot/app"` | App root |
| env.commandsPrefix | string | `""` | Dependabot comment command prefix |
| env.dependabotUrl | string | `""` | Optional app url, used for automated webhook creation |
| env.gitlabUrl | string | `"https://gitlab.com"` | Gitlab instance URL |
| env.http_proxy | string | `""` | Enable upstream http proxy |
| env.https_proxy | string | `""` | Enable upstream https proxy |
| env.logLevel | string | `"info"` | App log level |
| env.mongoDbUri | string | `""` | MongoDB URI |
| env.mongoDbUrl | string | `""` | MongoDB URL |
| env.no_proxy | string | `""` | Set proxy exceptions |
| env.redisUrl | string | `""` | Redis URL |
| env.sentryDsn | string | `""` | Optional sentry dsn for error reporting |
| env.sentryTracesSampleRate | string | `""` | Sentry traces sample rate |
| env.updateRetry | int | `2` | Update job retry count or 'false' to disable |
| fullnameOverride | string | `""` | Override fully qualified app name |
| image.imagePullSecrets | list | `[]` | Image pull secrets specification |
| image.pullPolicy | string | `"IfNotPresent"` | Image pull policy |
| image.repository | string | `"docker.io/andrcuns/dependabot-gitlab"` | Image to use for deploying |
| image.tag | string | `""` | Image tag |
| ingress.annotations | object | `{}` |  |
| ingress.enabled | bool | `false` | Enable ingress |
| ingress.hosts | list | `[]` |  |
| ingress.tls | list | `[]` |  |
| metrics.enabled | bool | `false` | Enable metrics endpoint for prometheus |
| metrics.service.type | string | `"ClusterIP"` | Metrics service type |
| metrics.serviceMonitor.additionalLabels | object | `{}` | Additional labels that can be used so ServiceMonitor resource(s) can be discovered by Prometheus |
| metrics.serviceMonitor.enabled | bool | `false` | Enable serviceMonitor |
| metrics.serviceMonitor.honorLabels | bool | `false` | Specify honorLabels parameter to add the scrape endpoint |
| metrics.serviceMonitor.metricRelabelings | list | `[]` | Metrics RelabelConfigs to apply to samples before ingestion |
| metrics.serviceMonitor.relabellings | list | `[]` | Metrics RelabelConfigs to apply to samples before scraping |
| metrics.serviceMonitor.scrapeInterval | string | `"30s"` | Metrics scrape interval |
| metrics.workerPort | int | `9394` | Worker metrics web server port |
| migrationJob.activeDeadlineSeconds | int | `180` | Job Active Deadline |
| migrationJob.backoffLimit | int | `4` | Job Back off limit |
| migrationJob.resources | object | `{}` | Migration job resource definitions |
| mongodb.auth.databases | list | `["dependabot_gitab"]` | MongoDB custom database |
| mongodb.auth.enabled | bool | `true` | Enable authentication |
| mongodb.auth.passwords | list | `["mongodb-password"]` | MongoDB custom user password |
| mongodb.auth.rootPassword | string | `""` | MongoDB root password |
| mongodb.auth.usernames | list | `["dependabot-gitlab"]` | MongoDB custom user username |
| mongodb.clusterDomain | string | `"cluster.local"` | Kubernetes Cluster Domain |
| mongodb.enabled | bool | `true` | Enable mongodb installation |
| mongodb.fullnameOverride | string | `"mongodb"` | String to fully override mongodb.fullname template |
| mongodb.service.port | int | `27017` | Mongodb service port |
| nameOverride | string | `""` | Override chart name |
| podSecurityContext | object | `{"fsGroup":1000,"runAsGroup":1000,"runAsUser":1000}` | Security Context |
| project_registration.cron | string | `""` | Cron expression of project registration cron job |
| project_registration.mode | string | `"manual"` | Project registration mode |
| project_registration.namespace | string | `""` | Allowed namespace expression for projects to register |
| projects | list | `[]` | List of projects to create/update on deployment |
| redis.architecture | string | `"standalone"` | Redis architecture. Allowed values: `standalone` or `replication` |
| redis.auth.enabled | bool | `true` | Enable authentication |
| redis.auth.password | string | `"redis-password"` | Redis password |
| redis.clusterDomain | string | `"cluster.local"` | Kubernetes Cluster Domain |
| redis.enabled | bool | `true` | Enable redis installation |
| redis.fullnameOverride | string | `"redis"` | Override redis name |
| registriesCredentials | object | `{}` | Credentials for private registries Example: PRIVATE_DOCKERHUB_TOKEN: token |
| service.annotations | object | `{}` | Service annotations |
| service.port | int | `3000` | Service pot |
| service.type | string | `"ClusterIP"` | Service type |
| serviceAccount.annotations | object | `{}` | Service account annotations |
| serviceAccount.name | string | `""` | Service account name |
| web.affinity | object | `{}` | Affinity |
| web.extraEnvVars | object | `{}` | Extra environment variables |
| web.extraVolumeMounts | object | `{}` | Extra volumeMounts for the pods |
| web.extraVolumes | object | `{}` | Extra volumes |
| web.livenessProbe.enabled | bool | `true` | Enable liveness probe |
| web.livenessProbe.failureThreshold | int | `5` | Liveness probe failure threshold |
| web.livenessProbe.periodSeconds | int | `10` | Liveness probe period |
| web.livenessProbe.timeoutSeconds | int | `2` | Liveness probe timeout |
| web.nodeSelector | object | `{}` | Node selectors |
| web.podAnnotations | object | `{}` | Pod annotations |
| web.replicaCount | int | `1` | Web container replicas count |
| web.resources | object | `{}` | Web container resource definitions |
| web.startupProbe.enabled | bool | `true` | Enable startup probe |
| web.startupProbe.failureThreshold | int | `12` | Startup probe failure threshold |
| web.startupProbe.initialDelaySeconds | int | `10` | Startup probe initial delay |
| web.startupProbe.periodSeconds | int | `10` | Startup probe period |
| web.startupProbe.timeoutSeconds | int | `3` | Startup probe timeout |
| web.tolerations | list | `[]` | Tolerations |
| web.updateStrategy | object | `{"type":"RollingUpdate"}` | Set up strategy for web installation |
| worker.affinity | object | `{}` | Affinity |
| worker.extraEnvVars | object | `{}` | Extra environment variables |
| worker.extraVolumeMounts | object | `{}` | Extra volumeMounts for the pods. This can be used for a netrc by mounting a secret at `/home/dependabot/.netrc`. This can be used for various package managers such as gomod |
| worker.extraVolumes | object | `{}` | Extra volumes |
| worker.livenessProbe.enabled | bool | `true` | Enable liveness probe |
| worker.livenessProbe.failureThreshold | int | `2` | Liveness probe failure threshold |
| worker.livenessProbe.periodSeconds | int | `120` | Liveness probe period |
| worker.livenessProbe.timeoutSeconds | int | `3` | Liveness probe timeout |
| worker.nodeSelector | object | `{}` | Node selectors |
| worker.podAnnotations | object | `{}` | Pod annotations |
| worker.probePort | int | `7433` | Health check probe port |
| worker.replicaCount | int | `1` | Worker container replicas count |
| worker.resources | object | `{}` | Worker container resource definitions |
| worker.startupProbe.enabled | bool | `true` | Enable startup probe |
| worker.startupProbe.failureThreshold | int | `12` | Startup probe failure threshold |
| worker.startupProbe.initialDelaySeconds | int | `10` | Startup probe initial delay |
| worker.startupProbe.periodSeconds | int | `5` | Startup probe period |
| worker.startupProbe.timeoutSeconds | int | `3` | Startup probe timeout |
| worker.tolerations | list | `[]` | Tolerations |
| worker.updateStrategy | object | `{"type":"RollingUpdate"}` | Set up strategy for worker installation |
